import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stack_page/provider/widget_provider.dart';
import 'package:stack_page/screen/home.dart';
import 'package:stack_page/screen/home2.dart';
import 'package:stack_page/screen/home3.dart';
import 'package:stack_page/screen/home_stack.dart';
import 'package:stack_page/widget/dart/navigator_bar.dart';

class HomePageState extends StatefulWidget {
  const HomePageState({Key? key}) : super(key: key);

  @override
  State<HomePageState> createState() => _HomePageStateState();
}

class _HomePageStateState extends State<HomePageState> {
  List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, WidgetProvider widgetProvider, child) {
        return DoubleBack(
          textStyle: TextStyle(color: Colors.deepOrange),
          // message: "sssss",
          // onConditionFail: (){
          //   print("sss");
          // },
          onFirstBackPress: (val) async {
            final isFirstRouteInCurrentTab = !await _navigatorKeys[widgetProvider.selectIndex].currentState!.maybePop();
            print(
              'isFirstRouteInCurrentTab: ' + isFirstRouteInCurrentTab.toString(),
            );
          },
          child: Scaffold(
            body: Stack(
              children: [
                _buildOffstageNavigator(0, widgetProvider.selectIndex),
                _buildOffstageNavigator(1, widgetProvider.selectIndex),
                _buildOffstageNavigator(2, widgetProvider.selectIndex),
              ],
            ),
            bottomNavigationBar: NavigatorStatePage(
              index: widgetProvider.selectIndex,
              selectIndex: (index) async {
                widgetProvider.selectIndex = index;
                _navigatorKeys[widgetProvider.selectIndex].currentState!.focusScopeNode.isFirstFocus;
                // final isFirstRouteInCurrentTab = !await _navigatorKeys[widgetProvider.selectIndex].currentState!.maybePop();
                if(_navigatorKeys[widgetProvider.selectIndex].currentState!.canPop()){
                   _navigatorKeys[widgetProvider.selectIndex].currentState!.popUntil((route) => route.isFirst);
                }
              },
            ),
          ),
        );
      },
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [
          Home1(
            onNext: _next,
          ),
          Home2(),
          Home3(),
        ].elementAt(index);
      },
    };
  }

  void _next() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeStack()));
  }

  Widget _buildOffstageNavigator(int indexPage, int indexSelect) {
    var routeBuilders = _routeBuilders(context, indexPage);

    return Offstage(
      offstage: indexSelect != indexPage,
      child: Navigator(
        key: _navigatorKeys[indexPage],
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name]!(context),
          );
        },
      ),
    );
  }
}
