import 'package:flutter/material.dart';

class WidgetProvider  extends ChangeNotifier{
  int _selectIndex = 0;


  int get selectIndex => _selectIndex;

  set selectIndex(int value) {
    _selectIndex = value;
    notifyListeners();
  }
}