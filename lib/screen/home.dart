import 'package:flutter/material.dart';
import 'package:stack_page/screen/home_stack.dart';
class Home1 extends StatefulWidget {
  final Function onNext;
  Home1({Key? key,required this.onNext}) : super(key: key);

  @override
  State<Home1> createState() => _Home1State();
}

class _Home1State extends State<Home1> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: TextButton(onPressed: (){
      widget.onNext();
    }, child: Text("go")));
  }
}
