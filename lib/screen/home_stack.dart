import 'package:flutter/material.dart';
import 'package:stack_page/screen/home_stack_go.dart';

class HomeStack extends StatefulWidget {
  const HomeStack({Key? key}) : super(key: key);

  @override
  State<HomeStack> createState() => _HomeStackState();
}

class _HomeStackState extends State<HomeStack> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrange,
      body: TextButton(onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeStackGo()));
      }, child: Text("go2")),
    );
  }
}
