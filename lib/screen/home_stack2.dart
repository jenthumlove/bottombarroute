import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home_stack3.dart';
class HomeStack2 extends StatefulWidget {
  const HomeStack2({Key? key}) : super(key: key);

  @override
  State<HomeStack2> createState() => _HomeStack2State();
}

class _HomeStack2State extends State<HomeStack2> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: TextButton(onPressed: (){
      Navigator.of(context).push( CupertinoPageRoute(builder: (context) {
        return HomeStack3();
      }));
    }, child: Text("go")));
  }
}
