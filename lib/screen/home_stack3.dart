import 'package:flutter/material.dart';

class HomeStack3 extends StatefulWidget {
  const HomeStack3({Key? key}) : super(key: key);

  @override
  State<HomeStack3> createState() => _HomeStack3State();
}

class _HomeStack3State extends State<HomeStack3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: TextButton(onPressed: (){
        Navigator.of(context).popUntil((route) => route.isFirst);
      }, child: Text("back"))),
    );
  }
}
