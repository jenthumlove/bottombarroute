import 'package:flutter/material.dart';

class HomeStackGo extends StatefulWidget {
  const HomeStackGo({Key? key}) : super(key: key);

  @override
  State<HomeStackGo> createState() => _HomeStackGoState();
}

class _HomeStackGoState extends State<HomeStackGo> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: TextButton(onPressed: (){
      Navigator.of(context).popUntil((route) => route.isFirst);
    }, child: Text("go")));
  }
}
