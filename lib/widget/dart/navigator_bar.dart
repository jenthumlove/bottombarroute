import 'package:flutter/material.dart';

class NavigatorStatePage extends StatefulWidget {
  Function selectIndex;
  int index;
   NavigatorStatePage({Key? key,required this.selectIndex,required this.index}) : super(key: key);

  @override
  State<NavigatorStatePage> createState() => _NavigatorStatePageState();
}

class _NavigatorStatePageState extends State<NavigatorStatePage> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business),
          label: 'Home2',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: 'Home3',
        ),
      ],
      currentIndex: widget.index,
      selectedItemColor: Colors.amber[800],
      onTap: (index){
        widget.selectIndex(index);
      },
    );
  }
}
